from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('add/', views.MatKulCreate.as_view(), name="Mata_Kuliah_Create"),
    path('list/', views.MatKulList.as_view(), name="Mata_Kuliah_List"),
    path('detail/<int:pk>/', views.MatKulDetail.as_view(), name="Mata_Kuliah_Detail"),
    path('delete/<int:pk>/', views.MatKulDelete.as_view(), name="Mata_Kuliah_Delete")
]
