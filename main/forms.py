from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit
from django import forms
from .models import MataKuliah


class AddMataKuliahForm(forms.ModelForm): # pragma: no cover
    semester = forms.ChoiceField(choices=[('Ganjil', 'Ganjil'), ('Genap', 'Genap')], label="Semester")
    tahun = forms.ChoiceField(choices=[(f'{tahun}/{tahun + 1}', f'{tahun}/{tahun + 1}') for tahun in range(2000, 2100)],
                              label="Tahun")

    def __init__(self, *args, **kwargs): # pragma: no cover
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'nama', 'dosen', 'jumlah_sks', 'deskripsi', 'ruang_kelas',
            Div(
                Div('semester', css_class='col-md-6', ),
                Div('tahun', css_class='col-md-6', ),
                css_class='row',
            ),
            FormActions(
                Submit('submit', 'Tambahkan', css_class='btn btn-primary btn-lg btn-block'),
            ),
        )

    class Meta:
        model = MataKuliah
        fields = ['nama', 'dosen', 'jumlah_sks', 'deskripsi', 'ruang_kelas', 'semester', 'tahun']
