from django.db import models


class MataKuliah(models.Model): # pragma: no cover

    nama = models.CharField(max_length=128)
    dosen = models.CharField(max_length=128)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=32)
    ruang_kelas = models.CharField(max_length=8)

