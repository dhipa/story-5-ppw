from django.contrib import messages
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import list, detail, edit

from .forms import AddMataKuliahForm
from .models import MataKuliah


def home(request): # pragma: no cover
    return redirect(reverse("main:Mata_Kuliah_List"))


class MatKulCreate(edit.CreateView): # pragma: no cover
    model = MataKuliah
    # fields = ['nama', 'dosen', 'jumlah_sks', 'deskripsi', 'ruang_kelas', 'semester', 'tahun']
    form_class = AddMataKuliahForm
    success_url = "/list"
    template_name = "main/add.html"

    def form_valid(self, form):
        form.instance.semester_tahun = form.cleaned_data['semester'] + " " + form.cleaned_data['tahun']
        return super(MatKulCreate, self).form_valid(form)


class MatKulList(list.ListView): # pragma: no cover
    model = MataKuliah
    template_name = "main/list.html"


class MatKulDetail(detail.DetailView): # pragma: no cover
    model = MataKuliah
    template_name = "main/detail.html"

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            messages.add_message(request, messages.ERROR,
                                 'Tidak ditemkan mata kuliah dengan id tersebut atau mata kuliah tersebut sudah dihapus')
            return redirect(reverse("main:Mata_Kuliah_List"))
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        pk = self.kwargs['pk']
        return super().get_context_data(**kwargs)


class MatKulDelete(edit.DeleteView): # pragma: no cover
    model = MataKuliah
    template_name = "main/delete.html"

    def get_context_data(self, **kwargs):
        pk = self.kwargs['pk']
        return super().get_context_data(**kwargs)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()

        try:
            self.object.delete()
        except Exception:
            messages.add_message(request, messages.ERROR, 'Tidak dapat menghapus objek')
            return HttpResponseRedirect(success_url)

        messages.add_message(request, messages.SUCCESS, 'Berhasil menghapus objek')

        return HttpResponseRedirect(success_url)

    success_url = reverse_lazy("main:Mata_Kuliah_List")
