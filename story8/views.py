from django.shortcuts import render
from django.http import JsonResponse
import requests


def search(request):
    url = f'https://www.googleapis.com/books/v1/volumes?q={request.GET["q"]}&maxResults=9'

    result = requests.get(url)
    return JsonResponse(result.json(), status=result.status_code)


def index(request):
    return render(request, "story8/index.html")
