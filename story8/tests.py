from django.test import TestCase
from django.urls import reverse


class MainTestCase(TestCase):
    def test_home_url_status_200(self):
        response = self.client.get(reverse('story8:home'))
        self.assertEqual(response.status_code, 200)

    def test_search_url_status_200(self):
        response = self.client.get(reverse('story8:search') + "?q=tes")
        self.assertEqual(response.status_code, 200)
