from django.test import TestCase
from django.urls import reverse


class MainTestCase(TestCase):
    def test_url_status_200(self):
        response = self.client.get(reverse('story7:home'))
        self.assertEqual(response.status_code, 200)